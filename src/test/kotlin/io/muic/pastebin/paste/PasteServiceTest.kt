package io.muic.pastebin.paste

import io.muic.pastebin.paste.exceptions.InvalidPasteFieldsException
import io.muic.pastebin.paste.exceptions.PasteNotFoundException
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*
import kotlin.math.pow

@ExtendWith(MockitoExtension::class)
@DisplayName("Test Pastebin Service Layer")
internal class PasteServiceTest {

    @Mock
    private lateinit var pasteRepository: PasteRepository

    @InjectMocks
    private lateinit var pasteService: PasteService

    private val paste = Paste(1, "valid title", "valid content", Date())
    private val validDAO = PasteDAO("valid title", "valid content")
    private val invalidDAO1 = PasteDAO(StringUtils.leftPad("", 513), "valid content")
    private val invalidDAO2 = PasteDAO("valid title", StringUtils.leftPad("", 2.0.pow(16).toInt(), "*")+1)
    private val beforeSave = validDAO.toEntity()


    @Test
    @DisplayName("Test retrieving pastes with existing and non-existing ids")
    fun getById() {
        `when`(pasteRepository.existsById(1)).thenReturn(true)
        `when`(pasteRepository.existsById(2)).thenReturn(false)
        `when`(pasteRepository.getOne(1)).thenReturn(paste)
        assertEquals(paste.toDTO(), pasteService.getById(1))
        assertThrows(PasteNotFoundException::class.java){ pasteService.getById(2) }
    }


    @Test
    @DisplayName("Test pasting with both valid and invalid inputs")
    fun paste() {
        `when`(pasteRepository.save(ArgumentMatchers.any(Paste::class.java))).thenReturn(paste)
        assertEquals(paste.toDTO(), pasteService.paste(validDAO))
        assertThrows(InvalidPasteFieldsException::class.java){ pasteService.paste(invalidDAO1) }
        assertThrows(InvalidPasteFieldsException::class.java){ pasteService.paste(invalidDAO2) }
    }

    @Test
    @DisplayName("Test fetching recent pastes")
    fun recents() {
        `when`(pasteRepository.findTop100ByOrderByCreatedAtDesc()).thenReturn(listOf(paste))
        assertEquals(listOf(paste).size, pasteService.recents().size)
    }

    @Test
    @DisplayName("Test existence of pastes on both existing and non-existing ids")
    fun exists() {
        `when`(pasteRepository.existsById(1)).thenReturn(true)
        `when`(pasteRepository.existsById(2)).thenReturn(false)
        assertEquals(pasteService.exists(1), true)
        assertEquals(pasteService.exists(2), false)
    }

    @Test
    @DisplayName("Test validity of inputs")
    fun isValid() {
        assertEquals(pasteService.isValid(validDAO), true)
        assertEquals(pasteService.isValid(invalidDAO1), false)
        assertEquals(pasteService.isValid(invalidDAO2), false)
    }

}