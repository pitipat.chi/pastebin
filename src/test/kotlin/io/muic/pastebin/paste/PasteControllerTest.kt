package io.muic.pastebin.paste

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.gson.GsonBuilderCustomizer
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.http.converter.json.GsonBuilderUtils
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MockMvcBuilder
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import kotlin.math.pow


@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("Test Pastebin Controller Layer")
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
internal class PasteControllerTest {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @Order(value = 1)
    @DisplayName("Test pasting with both valid and invalid inputs")
    fun paste() {
        val invalid1: PasteDAO = PasteDAO(StringUtils.leftPad("", 513), "valid content")
        val invalid2: PasteDAO = PasteDAO("valid title", StringUtils.leftPad("", 2.0.pow(16).toInt(), "*")+1)

        for (i in 1..200) {
            val valid: PasteDAO = PasteDAO("valid title$i", "valid content$i")
            mockMvc.perform(MockMvcRequestBuilders
                    .post("http://localhost:$port/api/paste")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Gson().toJson(valid)))
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$.id").value(i))
        }
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/api/paste")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Gson().toJson(invalid1)))
                .andExpect(status().isBadRequest)
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/api/paste")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Gson().toJson(invalid2)))
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(value = 2)
    @DisplayName("Test fetching paste on both existing and non-existing ids")
    fun getById() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/api/1"))
                .andExpect(status().isOk)
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/api/1000"))
                .andExpect(status().isNotFound)
                .andExpect(jsonPath("$").doesNotExist())
    }

    @Test
    @Order(value = 3)
    @DisplayName("Test fetching 100 recent pastes")
    fun recents() {
        val result: MvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/api/recents"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
        val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()
        val pastes: List<PasteDTO> = gson.fromJson(result.response.contentAsString, object : TypeToken<List<PasteDTO>>(){}.type)
        assertEquals(100, pastes.size)
    }
}