package io.muic.pastebin.configurations

import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration

@Configuration
@EnableCaching
class CachingConfiguration {
}