package io.muic.pastebin.paste.exceptions

import java.lang.RuntimeException

class InvalidPasteFieldsException(message: String = "either the title or content are too long") : RuntimeException(message)