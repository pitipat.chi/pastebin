package io.muic.pastebin.paste.exceptions

import java.lang.RuntimeException

class PasteNotFoundException(id: Long) : RuntimeException("Paste of id $id does not exists.")