package io.muic.pastebin.paste

import io.muic.pastebin.paste.exceptions.InvalidPasteFieldsException
import io.muic.pastebin.paste.exceptions.PasteNotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletableFuture
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class PasteController {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(PasteController::class.java)
    }

    @Autowired
    private lateinit var service: PasteService

    @GetMapping("/{id}")
    @Throws(PasteNotFoundException::class)
    fun getById(@PathVariable id: Long): ResponseEntity<PasteDTO> {
        if (!service.exists(id))
            throw PasteNotFoundException(id)
        return ResponseEntity(service.getById(id), HttpStatus.OK)
    }

    @PostMapping("/paste")
    @Throws(InvalidPasteFieldsException::class)
    fun paste(@Valid @RequestBody dao: PasteDAO, bindingResult: BindingResult): ResponseEntity<Any> {
        if (bindingResult.hasErrors())
            throw InvalidPasteFieldsException("Title or content should not be null/blank.")
        if (!service.isValid(dao))
            throw InvalidPasteFieldsException()
        val dto : PasteDTO = service.paste(dao)
        val body : MutableMap<String, Long?> = mutableMapOf()
        body["id"] = dto.id
        return ResponseEntity(body, HttpStatus.OK)
    }

    @PostMapping("/recents")
    fun recents(): ResponseEntity<List<PasteDTO>> {
        val dtos: List<PasteDTO> = service.recents()
        return ResponseEntity(dtos, HttpStatus.OK)
    }
}