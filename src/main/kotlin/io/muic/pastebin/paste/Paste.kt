package io.muic.pastebin.paste

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.GeneratorType
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@EntityListeners(AuditingEntityListener::class)
@Entity
@Table
class Paste (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @field:NotNull
        @field:NotBlank
        @field:Size(max=512)
        @Column(length = 512)
        var title: String? = null,

        @field:NotNull
        @Column(columnDefinition = "TEXT")
        var content: String? = null,

        @CreatedDate
        @Temporal(value = TemporalType.TIMESTAMP)
        var createdAt: Date? = null
) : Serializable


