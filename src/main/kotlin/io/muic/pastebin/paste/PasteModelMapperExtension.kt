package io.muic.pastebin.paste

import kotlin.reflect.full.memberProperties

fun Paste.toDTO() = with(::PasteDTO) {
    val propName = Paste::class.memberProperties.associateBy { it.name }
    callBy(parameters.associate {p ->
        p to when (p.name) {
            else -> propName[p.name]?.get(this@toDTO)
        }
    })
}

fun PasteDAO.toEntity() = with(::Paste) {
    val propName = PasteDAO::class.memberProperties.associateBy { it.name }
    callBy(parameters.associate {p ->
        p to when (p.name) {
            else -> propName[p.name]?.get(this@toEntity)
        }
    })
}
