package io.muic.pastebin.paste

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import java.util.*

data class PasteDTO(
        @JsonIgnore
        val id: Long?,
        val title: String?,
        val content: String?,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        val createdAt: Date?
) : Serializable