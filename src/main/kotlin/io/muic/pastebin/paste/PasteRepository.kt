package io.muic.pastebin.paste

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PasteRepository : JpaRepository<Paste, Long> {

    fun findTop100ByOrderByCreatedAtDesc(): List<Paste>

}