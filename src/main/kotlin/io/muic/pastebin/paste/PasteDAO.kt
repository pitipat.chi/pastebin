package io.muic.pastebin.paste

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class PasteDAO (
        @field:NotNull
        @field:NotBlank
        val title: String,
        @field:NotNull
        val content: String
)