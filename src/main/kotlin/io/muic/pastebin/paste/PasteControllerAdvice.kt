package io.muic.pastebin.paste

import io.muic.pastebin.paste.exceptions.InvalidPasteFieldsException
import io.muic.pastebin.paste.exceptions.PasteNotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.springframework.http.HttpStatus
import org.springframework.http.HttpHeaders
import org.springframework.web.context.request.WebRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import java.lang.IllegalArgumentException
import java.util.concurrent.ExecutionException


@ControllerAdvice
class PasteControllerAdvice : ResponseEntityExceptionHandler() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(PasteControllerAdvice::class.java)
    }
    @ExceptionHandler(value = [PasteNotFoundException::class])
    protected fun handleNotFound(ex: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.NOT_FOUND, request)
    }

    @ExceptionHandler(value = [InvalidPasteFieldsException::class])
    protected fun handleBadRequest(ex: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        val body : MutableMap<String, String?> = mutableMapOf()
        body["error"] = ex.message
        return handleExceptionInternal(ex, body, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }

    @ExceptionHandler(value = [ExecutionException::class, InterruptedException::class])
    protected fun handleThreadException(ex: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        val body : MutableMap<String, String?> = mutableMapOf()
        body["error"] = ex.message
        return handleExceptionInternal(ex, body, HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request)
    }

}