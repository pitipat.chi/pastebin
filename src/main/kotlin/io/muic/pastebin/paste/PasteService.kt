package io.muic.pastebin.paste

import io.muic.pastebin.paste.exceptions.InvalidPasteFieldsException
import io.muic.pastebin.paste.exceptions.PasteNotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutionException
import kotlin.math.pow

@Service
@CacheConfig(cacheNames = ["pastes"])
class PasteService {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(PasteService::class.java)
    }

    @Autowired
    private lateinit var repository: PasteRepository

    @Cacheable(key = "#id")
    @Throws(PasteNotFoundException::class, InterruptedException::class, ExecutionException::class)
    fun getById(id: Long): PasteDTO {
        if (!exists(id))
            throw PasteNotFoundException(id)
        return repository.getOne(id).toDTO()
    }

    @CacheEvict(key = "'recents'")
    @Throws(InterruptedException::class, ExecutionException::class)
    fun paste(dao: PasteDAO): PasteDTO {
        if (!isValid(dao))
            throw InvalidPasteFieldsException()
        val entity = dao.toEntity()
        return repository.save(entity).toDTO()
    }

    @Throws(InterruptedException::class, ExecutionException::class)
    @Cacheable(key = "'recents'")
    fun recents(): List<PasteDTO> {
        return repository.findTop100ByOrderByCreatedAtDesc().map { it.toDTO() }
    }

    fun exists(id: Long): Boolean {
        return repository.existsById(id)
    }

    fun isValid(dao: PasteDAO): Boolean {
        return dao.title.isNotEmpty() && dao.title.length <= 512 && dao.content.toByteArray().size <= (2.0.pow(16).toInt()-1)
    }

}